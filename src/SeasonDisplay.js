import React from 'react'

import './SeasonDisplay.css'

const getSeason = (lat,month)=>{
    if(month>2&&month<9){
        if(lat > 0){
            return 'Summer'
        }else{
            return 'Winter'
        }
    }else{
        if(lat>0){
            return 'Winter'
        }else{
            return 'Summer'
        }
    }
}

const SeasonConfig = {
    Summer:{
        text:'Lets hit the beach',
        iconName:'sun'
    },
    Winter:{
        text:'Its chilly out there',
        iconName:'snowflake'
    }
}


const SeasonDisplay = (props)=>{
    const season = getSeason(props.lat,new Date().getMonth())
    const {text,iconName} = SeasonConfig[season]
    return (
        <div className="container">
            <h3 className="mainHeading">Season Display</h3>
            <ul>
                <li>Latitude: {props.lat}</li>
                <li>Longitude: {props.long}</li>
            </ul>

            
            <h3 className="infoMsg">The current season for above co-ordinates is:{season}</h3>
            <i className={`massive ${iconName} icon`}></i>
            <h3 className="message">{text}</h3>
            <i className={`massive ${iconName} icon`}></i>
        </div>
    )
}

export default SeasonDisplay