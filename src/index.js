import React from 'react'
import ReactDOM from 'react-dom'

import SeasonDisplay from './SeasonDisplay'
import Spinner from './Spinner'



class App extends React.Component{

    state = { lat:0,long:0,errorMessage:'' }

    componentDidMount(){
        window.navigator.geolocation.getCurrentPosition((pos)=>{
            this.setState({
                lat:pos.coords.latitude,
                long:pos.coords.longitude
            })
        },(err)=>{
            this.setState({
                errorMessage:"Something went wrong while fetching the co-ordinates"
            })
            console.log(this.state.errorMessage)
        })
    }

    renderContent(){
        if(this.state.errorMessage && (!this.state.lat || !this.state.long)){
            return (
                <div className="mainContainer">
                    <h2>Hello Weather Application</h2>
                    <h3 style={{color:'red'}}>Error:{this.state.errorMessage}</h3>
                </div>
            )
        }
    
    
        if(!this.state.errorMessage && this.state.lat){
            return (
                <div className="mainContainer">
                    <h2>Hello Weather Application</h2>
                    <SeasonDisplay lat={this.state.lat} long={this.state.long}/>
                </div>
            )
        }
    
        return <Spinner message="Please accept location request"/>
    }

    //render is must for any class based component
    render(){
        return(
            <div className="blueBorder">
                {this.renderContent()}
            </div>
        )
    }
}

ReactDOM.render(<App />,document.querySelector("#root"))